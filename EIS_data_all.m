name     = "EIS_";
humidity = ["RH70","RH60","RH50"];
current  = ["7500"];
operating_system= 0; %SET OPERATING SYSTEM: 0=WINDOWS 1=MAC OS
if operating_system==0
    path = "C:\Users\Marco Sauermoser\OneDrive - NTNU\Experiments\Experimental Data\";
else
    path = "/Users/marcosauermoser/OneDrive - NTNU/Experiments/Experimental Data/";
end


date     = "11.03.2020\"; %CHANGE THIS ACCORDINGLY!
for i=1:length(humidity)
    for j=1:length(current)
        
        temp_name=strcat(name,humidity(i),"_",current(j));
        disp(temp_name)
        file_path=strcat(path,date,temp_name,".mpt");
        data.(matlab.lang.makeValidName(temp_name))=import_EIS_data(file_path);
               
    end
end
