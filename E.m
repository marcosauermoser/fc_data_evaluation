function z=E(p,f)% CPE
z=1./(p(1)*(1i*2*pi*f).^p(2));
end