
function z=p(varargin) % more zp in parallel
temp_size_varargin=size(varargin{1},1);
temp_n=ones(temp_size_varargin,nargin);
temp_sum=ones(temp_size_varargin,1);

for iii=1:nargin
    temp_n(:,iii)=varargin{iii};
end

for iii=1:temp_size_varargin
    temp_sum(iii)=sum(1./temp_n(iii,:));
end
z=1./(temp_sum);
end