function [lb_output,ub_output]=set_a_boundaries(lb,ub,circuit)
A=circuit~='p' & circuit~='s' & circuit~='(' & circuit~=')' & circuit~=',';
elements=circuit(A);

count=1;
for i=1:2:length(elements-2)
    ele_str=elements(i:i+1);
    %Columns: R=1, C=2, E=3, W=4
    if contains(ele_str,'2')
        count=count+1;
        if contains(ele_str,'E')
            lb(count)=0;
            ub(count)=1;

        end
    end
    count=count+1;
end
lb_output=lb;
ub_output=ub;