%Define setup matrix

setup=setup_full;

for i=1:(length(setup)/4)
   current_step(i)=setup(i*4-2);
   duration_step(i)=setup(i*4-1);   
end


duration_start=0;
j=1;
for i=1:length(duration_step)
    duration=duration_start+duration_step(i);
    if i>1
        if current_step(i)<current_step(i-1)
            hysteresis_index(j)=i;
            j=j+1;
        end
    end
    index_end=find(IV_data(:,1)==duration);
    index_start=find(IV_data(:,1)==(duration-30));
    
    voltage=IV_data(index_start:index_end,2);
    current=IV_data(index_start:index_end,3);
    voltage_average(i)=mean(voltage);
    voltage_s(i)=std(voltage);
    voltage_sigma(i)=voltage_s(i).^2;
    
    current_average(i)=mean(current);
    current_s(i)=std(current);
    current_sigma(i)=current_s(i).^2;
    duration_start=duration;
end
hysteresis_start=min(hysteresis_index);
cude=current_average.*1000./25;
power=current_average.*voltage_average;

figure(1)
hold on
errorbar(cude(1:hysteresis_start-1),voltage_average(1:hysteresis_start-1),voltage_s(1:hysteresis_start-1),'k-','LineWidth',1)
errorbar(cude(hysteresis_start:end),voltage_average(hysteresis_start:end),voltage_s(hysteresis_start:end),'r-','LineWidth',1)
hold off
legend('OCV->max. current','max. current->OCV')
lgd = legend;
lgd.Location='NorthEast';
xlabel('Current density [mA/cm^2]')
ylabel('Voltage [V]') 
set(gca,'FontSize',30)