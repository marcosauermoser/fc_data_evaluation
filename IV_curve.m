%Define setup matrix

setup=setup;

for i=1:(length(setup)/4)
   current_step(i)=setup(i*4-2);
   duration_step(i)=setup(i*4-1);   
end


duration_start=0;

for i=1:length(duration_step)
    duration=duration_start+duration_step(i);
    
    index_end=find(IV_data(:,1)==duration);
    index_start=find(IV_data(:,1)==(duration-30));
    
    voltage=IV_data(index_start:index_end,2);
    current=IV_data(index_start:index_end,3);
    voltage_average(i)=mean(voltage);
    voltage_s(i)=std(voltage);
    voltage_sigma(i)=voltage_s(i).^2;
    
    current_average(i)=mean(current);
    current_s(i)=std(current);
    current_sigma(i)=current_s(i).^2;
    duration_start=duration;
end

cude=current_average./25;
power=current_average.*voltage_average./25;

figure(2)
hold on
yyaxis left
errorbar(cude,voltage_average,3.*voltage_s,'k-','LineWidth',3)
yyaxis right
plot(cude,power,'r-','LineWidth',3)
hold off
legend('Voltage [V]', 'Power [W]')
lgd = legend;
lgd.Location='NorthWest';
xlabel('Current density [A/cm^2]')
yyaxis left
ylabel('Voltage [V]') 
set(gca,'FontSize',30)
yyaxis right
ylabel('Power Density [W/cm^2]')
set(gca,'FontSize',30)