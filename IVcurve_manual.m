endtimeRH70 = IVcurveRH70( startendRH70(:,2), 1 );
endtimeRH60 = IVcurveRH60( startendRH60(:,2), 1 );
endtimeRH50 = IVcurveRH50( startendRH50(:,2), 1 );

averagetimestartindexRH70 = zeros( length(endtimeRH70), 1 );
averagetimestartindexRH60 = zeros( length(endtimeRH60), 1 );
averagetimestartindexRH50 = zeros( length(endtimeRH50), 1 );

for i = 1:length(endtimeRH70)
    averagetimestartindexRH70(i,1) = find( IVcurveRH70(:,1) == round( (endtimeRH70(i,1)-30), 1 ) );
    averagetimestartindexRH60(i,1) = find( IVcurveRH60(:,1) == round( (endtimeRH60(i,1)-30), 1 ) );
    averagetimestartindexRH50(i,1) = find( IVcurveRH50(:,1) == round( (endtimeRH50(i,1)-30), 1 ) );
end

temp70=zeros(length(endtimeRH70),1);
temp60=zeros(length(endtimeRH60),1);
temp50=zeros(length(endtimeRH50),1);
for i = 1:length(endtimeRH70)
    temp70=IVcurveRH70(averagetimestartindexRH70(i):startendRH70(i,2),2);
    temp60=IVcurveRH60(averagetimestartindexRH60(i):startendRH60(i,2),2);
    temp50=IVcurveRH50(averagetimestartindexRH50(i):startendRH50(i,2),2);
    
    voltage_mean_RH70(i,1)=mean(temp70);
    voltage_mean_RH60(i,1)=mean(temp60);
    voltage_mean_RH50(i,1)=mean(temp50);
    
    voltage_std_RH70(i,1)=std(temp70);
    voltage_std_RH60(i,1)=std(temp60);
    voltage_std_RH50(i,1)=std(temp50);
    
    temp70=zeros(length(endtimeRH70),1);
    temp60=zeros(length(endtimeRH60),1);
    temp50=zeros(length(endtimeRH50),1);
end

cude_RH70=startendRH70(:,3)./25;
cude_RH60=startendRH60(:,3)./25;
cude_RH50=startendRH50(:,3)./25;

% cude_RH70(end)=mean(IVcurveRH70(averagetimestartindexRH70(end,1):startendRH70(end,2),3))/25;
% cude_RH60(end)=mean(IVcurveRH60(averagetimestartindexRH60(end,1):startendRH60(end,2),3))/25;
% cude_RH50(end)=mean(IVcurveRH50(averagetimestartindexRH50(end,1):startendRH50(end,2),3))/25;
figure(1)
hold on
plot(cude_RH70,voltage_mean_RH70, 'k')
plot(cude_RH60,voltage_mean_RH60, 'r')
plot(cude_RH50,voltage_mean_RH50, 'b')
hold off
legend('RH70','RH60','RH50')

