W_text={'R_d','tau'};
E_text={'Q','a'};
list_element={};
element_count=zeros(1,4);
count=1;
for i=1:2:length(elements-2)
    q2_str=elements(i:i+1);
    %Columns: R=1, C=2, E=3, W=4
    if contains(q2_str,'R')
        element_count(1)=element_count(1)+1;
    elseif contains(q2_str,'C')
        element_count(2)=element_count(2)+1;
    elseif contains(q2_str,'E')
        element_count(3)=element_count(3)+1;
    else
        element_count(4)=element_count(4)+1;
    end

    nlp=str2num(elements(i+1));
    if nlp==1
        if contains(q2_str,'R')
            q2=['Please enter ',q2_str(1),'_',char(string(element_count(1))),'_guess:  '];
            guess(count,1)=input(q2);
            count=count+1;
        else
            q2=['Please enter ',q2_str(1),'_',char(string(element_count(2))),'_guess:  '];
            guess(count,1)=input(q2);
            count=count+1;
        end   
    else
        if contains(q2_str,'W')
            for j=1:nlp
                q2=['Please enter ',char(W_text(j)),'_',char(string(element_count(4))),'_guess:  '];
                guess(count,1)=input(q2);
                count=count+1;
            end
        else
            for j=1:nlp
                q2=['Please enter ',char(E_text(j)),'_',char(string(element_count(3))),'_guess:  '];
                guess(count,1)=input(q2);
                count=count+1;
            end
        end
    end
end