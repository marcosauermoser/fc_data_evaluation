function Z_D_abs=absolute_Z_d(fq,R_d,tau_d)


omega = 2.*pi.*fq;

       
Z_d = R_d.*(tanh( sqrt( tau_d.*i.*omega ) ))./( sqrt( tau_d.*i.*omega ) ); 
Z_D_abs=abs(Z_d); 

    

end