function z=C(p,f)% capacitor
z=1i*2*pi*f*p;
z=1./z;
end