function [Re, Im, z, z_abs] = circuit_Z(para,freq,circuit_input)
param=para';

circuit=circuit_input;
% Start: Computes the complex impedance Z 
% process CIRCUIT to get the elements and their numeral inside CIRCUIT
A=circuit~='p' & circuit~='s' & circuit~='(' & circuit~=')' & circuit~=',';
element=circuit(A);
k=0;
% for each element
for i=1:2:length(element)
    k=k+1;
    nlp=str2num(element(i+1));% idendify its numeral
    localparam=param(1:nlp);% get its parameter values
    param=param(nlp+1:end);% remove them from param
    func=[element(i),'([',num2str(localparam,15),']',',freq)'];% buit an functionnal string
    z(:,k)=eval(func);% compute its impedance for all the frequencies
    % modify the initial circuit string to make it functionnal (when used
    % with eval)
    circuit=regexprep(circuit,element(i:i+1),['z(:,',num2str(k),')'],'once');
end
z=eval(circuit);% compute the global impedance
Re=real(z);
Im=imag(z);
z_abs=abs(z);
% real and imaginary parts are separated to be processed
% END of COMPUTECIRCUIT
end