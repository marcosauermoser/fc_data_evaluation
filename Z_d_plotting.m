%Z_d_plotting 

figure(1)
 hold on
 x_axis=[7.5,15,22.5];
 color=["k","b","g","r"];
 line=["-","--",":"];
 marker=["o","*","x","d"];
for k=1:length(date)
    for index1=1:length(humidity)
        for index2=1:length(current)
            
            temp_name=strcat(name,humidity(index1),"_",current(index2));
            temp_Z_d(index2)=abs_Z_d.(matlab.lang.makeValidName(date(k))).(matlab.lang.makeValidName(temp_name))(1);
        end
        plot(x_axis, temp_Z_d, strcat(line(index1),marker(index1),color(k)))
    end
end
hold off
legend('Serpentine RH70','Serpentine RH60','Serpentine RH50',...
       'Fractal a_i=1 a_o=1 RH70' ,'Fractal a_i=1 a_o=1 RH60','Fractal a_i=1 a_o=1 RH50',...
       'Fractal a_i=0.93 a_o=0.93 RH70','Fractal a_i=0.93 a_o=0.93 RH60','Fractal a_i=0.93 a_o=0.93 RH50',...
       'Fractal a_i=0.93 a_o=1 RH70','Fractal a_i=0.93 a_o=1 RH60','Fractal a_i=0.93 a_o=1 RH50')
title('|Z_d| 10kHz')
set(gca,'YScale','log')            
            
figure(2)
 hold on
 x_axis=[7.5,15,22.5];
 color=["k","b","g","r"];
 line=["-","--",":"];
 marker=["o","*","x","d"];
for k=1:length(date)
    for index1=1:length(humidity)
        for index2=1:length(current)
            
            temp_name=strcat(name,humidity(index1),"_",current(index2));
            temp_Z_d(index2)=abs_Z_d.(matlab.lang.makeValidName(date(k))).(matlab.lang.makeValidName(temp_name))(2);
        end
        plot(x_axis, temp_Z_d, strcat(line(index1),marker(index1),color(k)))
    end
end
hold off
legend('Serpentine RH70','Serpentine RH60','Serpentine RH50',...
       'Fractal a_i=1 a_o=1 RH70' ,'Fractal a_i=1 a_o=1 RH60','Fractal a_i=1 a_o=1 RH50',...
       'Fractal a_i=0.93 a_o=0.93 RH70','Fractal a_i=0.93 a_o=0.93 RH60','Fractal a_i=0.93 a_o=0.93 RH50',...
       'Fractal a_i=0.93 a_o=1 RH70','Fractal a_i=0.93 a_o=1 RH60','Fractal a_i=0.93 a_o=1 RH50')
title('|Z_d| 100Hz')
set(gca,'YScale','log')            
                       
figure(3)
 hold on
 x_axis=[7.5,15,22.5];
 color=["k","b","g","r"];
 line=["-","--",":"];
 marker=["o","*","x","d"];
for k=1:length(date)
    for index1=1:length(humidity)
        for index2=1:length(current)
            
            temp_name=strcat(name,humidity(index1),"_",current(index2));
            temp_Z_d(index2)=abs_Z_d.(matlab.lang.makeValidName(date(k))).(matlab.lang.makeValidName(temp_name))(3);
        end
        plot(x_axis, temp_Z_d, strcat(line(index1),marker(index1),color(k)))
    end
end
hold off
legend('Serpentine RH70','Serpentine RH60','Serpentine RH50',...
       'Fractal a_i=1 a_o=1 RH70' ,'Fractal a_i=1 a_o=1 RH60','Fractal a_i=1 a_o=1 RH50',...
       'Fractal a_i=0.93 a_o=0.93 RH70','Fractal a_i=0.93 a_o=0.93 RH60','Fractal a_i=0.93 a_o=0.93 RH50',...
       'Fractal a_i=0.93 a_o=1 RH70','Fractal a_i=0.93 a_o=1 RH60','Fractal a_i=0.93 a_o=1 RH50')
title('|Z_d| 100Hz')
set(gca,'YScale','log')             
            