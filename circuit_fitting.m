% This script loads experimental data from .mpt files (exported from
% EC-Lab). The curve fit will then be done based on the entered circuit
% It is important to change the file path of the folder, where all data is
% collected and sorted e.g. after the date of the experiment.
clear all
clc
close all
%% Define which circuit to use (taken from Jean-Luc Dellis (2020). Zfit
%%(https://www.mathworks.com/matlabcentral/fileexchange/19460-zfit), MATLAB Central File Exchange. Retrieved January 30, 2020)
%                   It has to be composed of the operators S and/or P which put elements
%                   in series (lower S) or in parallel (lower P). For instances:
%                   'p(R1,C1)' defines an resistor R and an capacitor C in parallel.
%                   's(R1,p(R1,C1))' a R//C parallel circuit is put in series with a resistor.
%                   's(p(R1,C1),p(R1,C1))' 2-R//C circuits are put in series.
%                   'p(R1,C1,E2)' a CPE is put in parallel with a R//C circuit.
%                   'E2' is a CPE alone
%                   'W2' is a Warburg diffusion element (finite)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    circuit='s(R1,p(E2,R1),p(E2,s(R1,W2)))';%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Import EIS Data
% Enter information according to your log files.


name     = "EIS_";
humidity = ["RH70","RH60","RH50"];
current  = ["7500","15000","22500"];
date     = ["04.03.2020"];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% CHANGE THE PATHS TO EXPERIMENTAL DATA ACCORDING TO YOUR SYSTEM
operating_system = 1; %SET OPERATING SYSTEM: 0=WINDOWS 1=MAC OS
if operating_system==0
    path = "C:\Users\Marco Sauermoser\OneDrive - NTNU\Experiments\Experimental Data\";
else
    path = "/Users/marcosauermoser/OneDrive - NTNU/Experiments/Experimental Data/";
end
count=1;
for k=1:length(date)
    for i=1:length(humidity)
        for j=1:length(current)
    
            temp_name=strcat(name,humidity(i),"_",current(j))
            %disp(strcat(date,".",temp_name))
            file_path=strcat(path,date(k),"/",temp_name,".mpt");
            temp=import_EIS_data_curve_fitting(file_path);
            f(:,count)=temp(:,1);
            Re(:,count)=temp(:,2);
            Im(:,count)=temp(:,3);
            Z(:,count)=temp(:,3);
            description(1,count)=temp_name;
            count = count+1;
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Detect anomalies with mutlivariate Gaussian distribution
% It is important to enter manually one measurement set for cross
% validation set. If no validation set is used, a fixed epsilon value for
% cutoff is picked
close all
% epsilon=0.03;
% %Use log(f) and Z as features for the distribution
% X=[log(f(:)),Z(:)];
% 
% [mu,Sigma2]=estimateGaussian(X);
% p=multivariateGaussian(X,mu,Sigma2);
% 
% outliers=find(p<epsilon);
Re_array=Re(:);
Im_array=Im(:);
f_array=f(:);
Z_array=Z(:);
order=linspace(1,length(Re(:,1)),length(Re(:,1)));
outliers=find(isoutlier(Re,'movmedian',10,'SamplePoints',order)==true | isoutlier(Im,'movmedian',10,'SamplePoints',order)==true);
%outliers=find(isoutlier(Z_array)==true);

hold on
scatter(Re_array,Im_array)
plot(Re_array(outliers), Im_array(outliers), 'ro', 'LineWidth', 2, 'MarkerSize', 10);
hold off
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Calculate the amount of needed parameters based on the given circuit
A=circuit~='p' & circuit~='s' & circuit~='(' & circuit~=')' & circuit~=',';
elements=circuit(A);
n_para=0;
w_index=[];
for i=1:2:length(elements-2)
   n_para=n_para+str2num(elements(i+1));
   if contains(elements(i),'W')
       w_index=[w_index,n_para-1,n_para];
   end
end

if contains(elements(1:2),'R1')
    R1_exists=true;
else
    R1_exists=false;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Find approximate R1 if available in circuit
% This algorithm tries to find the intersectant with the Z_re axis

[R1_guess,R1_ub,R1_lb]=find_R1(Re,Im);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set guesses, upper and lower limits for minimization. NEEDS TO FIT TO CIRCUIT DEFINITION

q1 = ['_____________________________________________',newline,...
    'Which option for the guess of the curve fit do you want to use:',newline,...
            '1  Enter manually in command window',newline,...
            '2  Enter guess array directly in script (need to be according to entered circuit)',newline,...
            '3  Random guess',newline,...
            'Respond:  '];
a1 = input(q1);

if a1==1
    input_guess_script
    guess=guess.*ones(length(guess),length(description));
elseif a1==2
guess = [
         0.004;...   % R1_guess  
         0.535;...     % Q_1_guess
         1;...       % a_1_guess
         0.00449;...   % R_2_guess 
         2.145;...       % Q_2_guess
         0.65;...     % a_2_guess
         0.004655;...% R_3_guess
         0.001649;...   % R_d_1_guess
         0.29         % t_d_1_guess
        ];
elseif a1==3
    %get_random_guess
    guess=rand(n_para,1);
else
    disp('Incorrect input!')
    return
end

q2_options= {};
q2 = ['_____________________________________________',newline,...
      'Which option for the boundaries of the curve fit do you want to use:',newline,...
            '1  Enter boundaries manually in command window',newline,...
            '2  Enter boundary arrays directly in script (need to be according to entered circuit)',newline,...
            '3  Use 0 as lower and Inf as upper boundary',newline,...
            'Respond:  '];
        
a2 = input(q2);

if a2==1
    disp('_____________________________________________')
    disp('Upper boundary values')
    
    input_ub_script
    ub=ub.*ones(length(ub),length(description));
    disp('_____________________________________________')
    disp('Lower boundary values')
    input_lb_script
    lb=lb.*ones(length(lb),length(description));
elseif a2==2
ub = [
               inf;...   % R1_upper  
               inf;...       % Q_1_upper
               1.0;...       % a_1_upper
               inf;...   % R_2_upper
               100;...       % Q_2_upper
               1;...     % a_2_upper
               inf;...% R_3_upper
               inf;...   % R_d_1_upper
               inf         % t_d_1_upper
              ];

lb = [
               -inf;...   % R1_lower  
               0;...     % Q_1_lower
               0.5;...       % a_1_lower
               0;...   % R_2_lower
               0;...       % Q_2_lower
               0.5;...     % a_2_lower
               0;...% R_3_lower
               0;...   % R_d_1_lower
               0         % t_d_1_lower
              ];
elseif a2==3
    ub=ones(n_para,1)*Inf;
    lb=zeros(n_para,1);
else
    disp('Incorrect input!')
    return
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%



%% Perform the curve fit
n=length(Re(:,1));
outliers_temp=outliers;
for i=1:length(description)
    % if there is now guess given, create a randomized array
    
%     exitflag=-1;
%     while exitflag<=0
%         %If no upper and lower boundaries are given, use fsolve with
%         %Levenberg-Marquardt algorithm
%         if isempty(ub) && isempty(lb)
%             options = optimset('Algorithm','levenberg-marquardt','Diagnostics', 'on', 'Display','iter');
%             [para(:,i),fval,exitflag] = fsolve(@circuit_res, guess, options, [f(:,i),Re(:,i),Im(:,i),Z(:,i)],circuit);
%             
%             guess=para(:,i);
%         else
%             options= optimset('Algorithm','sqp','Diagnostics', 'on', 'Display', 'iter', ...
% 		      'MaxFunEvals',1E12,'MaxIter',1e5,'TolCon',1e-7,'TolFun',1e-7);
%             [para(:,i),fval,exitflag,~] = fmincon(@circuit_res,guess,[],[],[],[],[],lb,ub,options,...
%                                           [f(:,i),Re(:,i),Im(:,i),Z(:,i)],circuit);
%             guess=para(:,i);
%         end~
%     end
    exitflag=-1;
    Re_filtered=Re_array((i-1)*n+1:i*n);
    Im_filtered=Im_array((i-1)*n+1:i*n);
    f_filtered=f_array((i-1)*n+1:i*n);
    Z_filtered=Z_array((i-1)*n+1:i*n);
    if ~isempty(outliers_temp)
    Re_filtered(outliers_temp(outliers_temp<=n & outliers_temp>0)) =[];
    Im_filtered(outliers_temp(outliers_temp<=n & outliers_temp>0)) =[];
    f_filtered(outliers_temp(outliers_temp<=n & outliers_temp>0)) =[];
    Z_filtered(outliers_temp(outliers_temp<=n & outliers_temp>0)) =[];
    outliers_temp=outliers_temp-n;
    end
    
    if R1_exists
        guess(1)=R1_guess(:,i);
        lb(1)=R1_lb(:,i);
        ub(1)=R1_ub(:,i);
    end
    %Set lb and ub for a values of CPE elements to 0 and 1
    [lb,ub]=set_a_boundaries(lb,ub,circuit);
    
    while exitflag<=0
        [para(:,i),Z_fit,~,exitflag,~]=Zfit([f_filtered,Re_filtered,-Im_filtered],'z',circuit,guess',[],'fitNP',lb',ub');
        disp(exitflag)
        guess=para(:,i);
    end
    
    
    if length(Z_fit(:,1))<length(Re(:,i))
        Z_fit(end+1:length(Re(:,i)),:)=NaN;
    end
    Re_fit(:,i)=Z_fit(:,1);
    Im_fit(:,i)=Z_fit(:,2);
    Z_abs_fit(:,i)=abs(complex(Re_fit(:,i),Im_fit(:,i)));
    
    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Calculation of absulolte Impedance of Warburg element 
% (if Warburg elements are contained in the circuit)) 
% row= diffeent Warburg elements in the circuit
% column = different experiments
if ~isempty(w_index)
   for i=1:length(w_index)/2 
       for j=1:length(description)
            abs_Z_d_10000(i,:)= absolute_Z_d(10e3,para(i,:),para(i+1,:));
            abs_Z_d_100(i,:)= absolute_Z_d(100,para(i,:),para(i+1,:));
            abs_Z_d_0_1(i,:)= absolute_Z_d(0.1,para(i,:),para(i+1,:));
       end
   end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Curve fitting for one specific dataset
% k=3            
% exitflag=-1;
%             guess=rand(length(guess),1);
%             temp_name=strcat(name,humidity(3),"_",current(1));
%             data=EIS.(matlab.lang.makeValidName(date(k))).(matlab.lang.makeValidName(temp_name));
%             while exitflag<=0
%             %[para,fval,exitflag,] = fminunc(@circuit_res,guess,options,data);
%             [para.(matlab.lang.makeValidName(date(k))).(matlab.lang.makeValidName(temp_name)), fval,exitflag,~] = fmincon(@circuit_res,guess,[],[],[],[],lower_limit,upper_limit,[],options,data);
%             guess=para.(matlab.lang.makeValidName(date(k))).(matlab.lang.makeValidName(temp_name));
%             end
 
%  %% Plotting of data
%  figure(1)
%  hold on
% for i=1:length(description)
%     scatter(Re(:,i),Im(:,i))
%     plot(Re_fit(:,i),Im_fit(:,i))
% end
% hold off
%             
            
            
            
            
            
            
            
            
            
            
            
            
            