% Calculate gas usage 
% clc,clear
%%
% Import log data (example name: 20190318.log). Import the following three columns:
% A, G, H (ctrl click the heading letters to mark the entire column).
% Select output type to be: "String Array", and press 
% the Check button (Import selected Data into MATLAB workspace)

% If you have more than one log, import them subsequently. 

%% 
%%%%% After importing, run program. %%%%%
%%
myVars = who;
%%
for i = 1:length(myVars)
    rawData{i} = evalin('base', string(myVars{i}));
end

%%
k = 0;
j = 0;
for m = 1:length(rawData)
    for i = 1:length(rawData{m})
        tempdata = rawData{m};
        if contains(tempdata(i,1), 'Date: ')
            k = k+1;
            Date{k} = erase(tempdata(i,1), 'Date: ');
        end
        if contains(tempdata(i,1), 'Heure: ')
            j = j+1;
            Hour{j} = erase(tempdata(i,1), 'Heure: ');
        end
    end
end
clearvars tempdata
%%
Data = [];

for m = 1:length(rawData)
    n = 0;
    tempdata = rawData{m};
    maxLength = length(tempdata);
    i = 0;
    while i < maxLength-1
        i = i+1;
        if contains(tempdata(i,1), 'Heure') & ~contains(tempdata(i+1,1), 'Time') 
            n=n+1; 
            indax(m,n) = i+1;
        end
    end
    if n == 0
        i = 0;
        while i < maxLength-1
        i = i+1;
        if contains(tempdata(i,1), 'Heure') & contains(tempdata(i+1,1), 'Time') 
            n=n+1; 
            indax(m,n) = i+2;
        end
        end
    end
%     tempdata;
    Databig{m} = tempdata(indax(m,n):end,:);
end
dataTot = cat(1,Databig{:});
%%
Data = replace(dataTot, 'E', 'e');
Data = replace(Data, ',', '.');
%%
for i = 1:length(Data)
    times(i) = str2num(Data(i,1))/60; % min
    H2_flow(i) = str2num(Data(i,2))/1000; %L/min
    O2_flow(i) = str2num(Data(i,3))/1000; %L/min
end
H2_flow = H2_flow';
O2_flow = O2_flow';
times = times';
% plot(times)
%%
%
clc
color = {'', '--b', 'g'};
prevInd = 1;
lognr = 1;

for m = 1:length(Databig)-1
    indrux(m) = length(Databig{lognr}) + prevInd(m);
    times(indrux(m):indrux(m)+length(Databig{lognr+1})-1) = times(indrux(m):indrux(m)+length(Databig{lognr+1})-1)+times(indrux(m)-1);
    prevInd(m+1) = indrux(m);
    lognr = lognr + 1;
end

accum_H2(1) = mean(H2_flow(1:2))*(times(2)-times(1));
accum_O2(1) = mean(O2_flow(1:2))*(times(2)-times(1));
for i = 2:length(times)
    accum_H2(i) = trapz(times(1:i), H2_flow(1:i));
    accum_O2(i) = trapz(times(1:i), O2_flow(1:i));
end

accum_H2 = accum_H2';
accum_O2 = accum_O2';

%%
leg = {''};
hf=figure();
set(hf, 'Position', [350 370 650 450])
hold on
plot(times, H2_flow, 'b', 'linewidth', 1.5), leg = [leg, 'H2_{flow}'];
hold on
plot(times, O2_flow, 'r', 'linewidth', 1.5), leg = [leg, 'O2_{flow}'];

% ylim([0 0.7])
ylabel('Gas usage [L min^{-1}]')
xlabel('Time [min]')

for i = 1:length(Date)
    datenums(i) = datenum(Date{i}, 'dd.mm.yyyy');
end
[tmp minDateInd] = min(datenums);
[tmp maxDateInd] = max(datenums);
clearvars tmp

title(['Gas usage: ' Date{minDateInd} ' to ' Date{maxDateInd}])
yyaxis right
yl=ylabel('Accumulated gas usage [L]'); yl.Color = [0 0 0];
yax=gca; yax.YColor = [0 0 0];
plot(times, accum_H2, '--b'), leg = [leg, 'H2_{accum}'];
plot(times,accum_O2, '--r'), leg = [leg, 'O2_{accum}'];

ylim([0 1.1*max([accum_H2(end), accum_O2(end)])])

hl = legend(leg{2:end}, 'location', 'southoutside', 'orientation', 'horizontal');

H2display = sprintf('H_2 usage: %.2f L over %.2f hours', round(accum_H2(end), 2), round(times(end)/60,2));
O2display = sprintf('O_2 usage: %.2f L over %.2f hours', round(accum_O2(end), 2), round(times(end)/60,2));
disp(H2display)
disp(O2display)

% set(gcf, 'PaperPositionMode', 'auto');

%% Save figure to pdf
% figHandles = get(groot, 'Children');
% 
% fignames = {['Gas_usage_' char(Date{1}) '.pdf']};
% for i = 1:length(figHandles)
%     figure(i)
%             set(gca,'box','off')
%             saveas(gcf, fignames{i}, 'pdf');
% end

