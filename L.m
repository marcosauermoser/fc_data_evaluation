function z=L(p,f)% inductor
z=1i*2*pi*f*p;
end