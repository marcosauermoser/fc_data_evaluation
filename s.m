function z=s(varargin) % more zs in series
temp_size_varargin=size(varargin{1},1);
temp_n=ones(temp_size_varargin,nargin);
temp_sum=ones(temp_size_varargin,1);

for iii=1:nargin
    temp_n(:,iii)=varargin{iii};
end

for iii=1:temp_size_varargin
    temp_sum(iii)=sum(temp_n(iii,:));
end
z=temp_sum;
end