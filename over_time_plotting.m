figure(1)
hold on
yyaxis left
plot(seconds(Breakin_log(:,1)),Breakin_log(:,5),'DurationTickFormat','hh:mm:ss')
yyaxis right
plot(seconds(Breakin_log(:,1)),Breakin_log(:,6),'DurationTickFormat','hh:mm:ss')
hold off

%%
figure(2)
hold on
yyaxis left
plot(seconds(IVcurve(:,1)),IVcurve(:,2),'DurationTickFormat','hh:mm:ss')
yyaxis right
plot(seconds(IVcurve(:,1)),IVcurve(:,3),'DurationTickFormat','hh:mm:ss')
hold off

%%
figure(2)
hold on
yyaxis left
plot(seconds(Breakin(:,1)),Breakin(:,2),'DurationTickFormat','hh:mm:ss')
yyaxis right
plot(seconds(Breakin(:,1)),Breakin(:,3),'DurationTickFormat','hh:mm:ss')
hold off
