function z=W(p,f)%Warburg
z=p(1).*(tanh(sqrt(p(2).*1i.*2*pi*f)))./(sqrt(p(2).*1i.*2*pi*f));        
end