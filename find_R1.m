function [R1_guess,R1_ub,R1_lb]=find_R1(Re,Im)

Re_R1=Re(1:6,:);
Im_R1=Im(1:6,:);

order=linspace(1,length(Re_R1(:,1)),length(Re_R1(:,1)));
outliers_R1=find(isoutlier(Re_R1,'SamplePoints',order)==true | isoutlier(Im_R1,'SamplePoints',order)==true);

outliers_R1_temp=outliers_R1;
n_R1=length(Re_R1(:,1));
Re_R1_array=Re_R1(:);
for i=1:length(Re_R1(1,:))
    Re_R1_filtered=Re_R1_array((i-1)*n_R1+1:i*n_R1);
    if ~isempty(outliers_R1_temp)
        Re_R1_filtered(outliers_R1_temp(outliers_R1_temp<=n_R1 & outliers_R1_temp>0)) =[];
    end
    R1_guess(:,i)=mean(Re_R1_filtered);
    outliers_R1_temp=outliers_R1_temp-n_R1;
end
R1_ub = R1_guess.*1.05;
R1_lb = R1_guess.*0.95;

end